// Import things
const { Client, MessageEmbed } = require("discord.js");
const client = new Client();
const ytdl = require("ytdl-core-discord");
const { prefix, token, key, clientId, clientSecret } = require("./config.json");
const { decode } = require("html-entities");
const SpotifyWebApi = require("spotify-web-api-node");
const google = require("googleapis");
const youtubeV3 = new google.youtube_v3.Youtube({ version: "v3", auth: key });

// Youtube regex
function ytVidId(url) {
  var p =
    /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
  return url.match(p) ? RegExp.$1 : false;
}
function ytPlaylistId(url) {
  var p =
    /(?:https?:\/\/)?(?:www\.)?youtube\.com\/playlist\?list=([a-zA-Z_0-9]+)/;
  return url.match(p) ? RegExp.$1 : false;
}
// Spotify regex
let spotifyRegex = /^(?:https?:\/\/)?open\.spotify\.com\/track\/([a-zA-Z0-9]+)/;
// Stuff for the commands
let connection;
let dispatcher;
let queue = [];
let disconnectToken;
let stream;

// Spotify api stuff
var spotifyApi = new SpotifyWebApi({
  clientId: clientId,
  clientSecret: clientSecret,
});
function spotifyEveryHour() {
  spotifyApi.clientCredentialsGrant().then(
    function (data) {
      console.log("Token refreshed!");
      console.log("The access token is " + data.body["access_token"]);
      spotifyApi.setAccessToken(data.body["access_token"]);
    },
    function (err) {
      console.log("Something went wrong!", err);
    }
  );
}
spotifyEveryHour();
setInterval(spotifyEveryHour, 3600000);

// Print 'Ready!' when bot is all ready to be used
client.once("ready", () => {
  console.log("Ready!");
});

// Commands down here
client.on("message", async (message) => {
  // Don't run anything if message isn't in a server
  if (!message.guild) return;
  // Ping command
  if (message.content === `${prefix}ping`) {
    message.channel.send("Pong lol");
  // Pause command, stays above play so it runs before it (so resuming with the play command works)
  } else if (message.content === `${prefix}pause`) {
    if (connection == undefined)
      return message.channel.send(
        "You need to be connected to a channel to run this!"
      );
    await dispatcher.pause();
    message.react("⏸");
  // Play command
  } else if (
    message.content.startsWith(`${prefix}play`) ||
    message.content.startsWith(`${prefix}p`)
  ) {
    clearTimeout(disconnectToken);
    if (message.member.voice.channel == undefined) {
      message.channel.send(
        "You need to be connected to a channel to run this!"
      );
      return;
    }
    let searchQuery = message.content.split(` `).slice(1).join(` `);
    // Resumes from a pause if no search query is attached to play command
    if (!searchQuery) {
      if (connection != undefined && dispatcher == undefined) {
        onStreamEnd();
        clearTimeout(disconnectToken);
      } else if (connection == undefined) {
        return message.channel.send(
          "You need to be connected to a channel to run this!"
        );
      }
      await dispatcher.resume();
      message.react("▶️");
      return;
    }
    if (connection == undefined) {
      connection = await message.member.voice.channel.join();
      queue = [];
    }
    let youtubeResult;
    if (ytVidId(searchQuery)) {
      youtubeResult = await youtubeV3.videos.list({
        id: ytVidId(searchQuery),
        maxResults: 1,
        part: "snippet",
      });
      youtubeResult = youtubeResult.data.items[0];
      youtubeResult.snippet.link =
        "https://www.youtube.com/watch?v=" + youtubeResult.id;
      youtubeResult = youtubeResult.snippet;
      queue.push(youtubeResult);
    } else if (ytPlaylistId(searchQuery)) {
      youtubeResult = await youtubeV3.playlistItems.list({
        playlistId: ytPlaylistId(searchQuery),
        maxResults: 50,
        part: "snippet",
      });
      let playlistEntries = youtubeResult.data.items.map((v) => {
        v.snippet.link =
          "https://www.youtube.com/watch?v=" + v.snippet.resourceId.videoId;
        return v.snippet;
      });
      youtubeResult = playlistEntries[0];
      queue.push(...playlistEntries);
    } else {
      if (spotifyRegex.test(searchQuery)) {
        let regexResult = spotifyRegex.exec(searchQuery);
        let spotifyData = await spotifyApi.getTrack(regexResult[1]);
        searchQuery =
          spotifyData.body.name + " " + spotifyData.body.artists[0].name;
      }
      youtubeResult = await youtubeV3.search.list({
        type: "video",
        part: "snippet",
        maxResults: 1,
        q: searchQuery,
      });
      youtubeResult = youtubeResult.data.items[0];
      youtubeResult.snippet.link =
        "https://www.youtube.com/watch?v=" + youtubeResult.id.videoId;
      youtubeResult = youtubeResult.snippet;
      queue.push(youtubeResult);
    }
    if (dispatcher == undefined) {
      stream = await ytdl(youtubeResult.link, {
        filter: "audioonly",
      });
      dispatcher = await connection.play(stream, {
        type: "opus",
      });
      dispatcher.on("finish", onStreamEnd);
    }
    // Makes embed
    const embed = new MessageEmbed()
      .setAuthor("Added to queue:")
      .setTitle(`${decode(youtubeResult.title)}`)
      .setColor(0xff0000)
      .setDescription(
        `[${decode(
          youtubeResult.channelTitle
        )}](https://www.youtube.com/channel/${youtubeResult.channelId})`
      )
      .setURL(youtubeResult.link)
      .setThumbnail(youtubeResult.thumbnails.high.url);
    message.react("👍");
    message.channel.send(embed);
  } else if (
    message.content === `${prefix}disconnect` ||
    message.content === `${prefix}dc`
  ) {
    // Disconnect command
    if (connection == undefined)
      return message.channel.send(
        "You need to be connected to a channel to run this!"
      );
    queue = [];
    connection.disconnect();
    connection = undefined;
    message.react("👋");
  } else if (message.content === `${prefix}join`) {
    // Joins whichever voice channel the sender is in, if any
    if (connection != undefined)
      return message.channel.send("I'm already in a channel dummy lol");
    connection = await message.member.voice.channel.join();
  } else if (message.content === `${prefix}skip`) {
    // Skips to the next song in the queue
    if (connection == undefined)
      return message.channel.send(
        "You need to be connected to a channel to run this!"
      );
    await dispatcher.end();
    message.react("👍");
  } else if (
    message.content === `${prefix}queue` ||
    message.content === `${prefix}q`
  ) {
    // Displays the current queue
    if (queue.length > 1) {
      const embed = new MessageEmbed()
        .setTitle("Current queue:")
        .setColor(0xff0000)
        .setDescription(
          queue
            .slice(1, 10)
            .map((v) => `• [${decode(v.title)}](${v.link})`)
            .join("\n")
        );
      message.channel.send(embed);
    } else if (queue.length == 0) {
      const embed = new MessageEmbed()
        .setTitle("There's nothing in the queue!")
        .setColor(0xff0000);
      message.channel.send(embed);
    } else if (queue.length == 1) {
      const embed = new MessageEmbed()
        .setTitle("There's nothing up next!")
        .setColor(0xff0000);
      message.channel.send(embed);
    }
  } else if (message.content === `${prefix}np`) {
    // Displays the currently playing song
    let dat = queue[0];
    if (dat == undefined)
      return message.channel.send("There's nothing in the queue!");
    const embed = new MessageEmbed()
      .setAuthor("Now playing:")
      .setTitle(decode(dat.title))
      .setColor(0xff0000)
      .setDescription(
        `[${decode(dat.channelTitle)}](https://www.youtube.com/channel/${
          dat.channelId
        })`
      )
      .setURL(dat.link)
      .setThumbnail(dat.thumbnails.high.url);
    message.channel.send(embed);
  } else if (message.content === `${prefix}resume`) {
    // Resumes from pausing
    if (connection == undefined)
      return message.channel.send(
        "You need to be connected to a channel to run this!"
      );
    await dispatcher.resume();
    message.react("▶️");
  } else if (message.content === `${prefix}clear`) {
    if (connection == undefined)
      return message.channel.send(
        "You need to be connected to a channel to run this!"
      );
    queue = queue.slice(0, 1);
    const embed = new MessageEmbed()
      .setTitle("Queue cleared!")
      .setColor(0xff0000);
    message.channel.send(embed);
  } else if (message.content.startsWith(`${prefix}remove `)) {
    if (connection == undefined)
      return message.channel.send(
        "You need to be connected to a channel to run this!"
      );
    let removeThing = parseInt(message.content.split(` `).slice(1).join(` `));
    if (isNaN(removeThing))
      return message.channel.send("You didn't type a number lmao");
    if (removeThing != 0) queue.splice(removeThing, 1);
    else
      return message.channel.send(
        "You can't remove the currently playing song..."
      );
    message.react("👍");
  }
});

function disconnect() {
  connection.disconnect();
}

async function onStreamEnd() {
  // Stream ending function
  queue.shift();
  if (queue[0] == undefined) {
    disconnectToken = setTimeout(disconnect, 10000);
    dispatcher = undefined;
    return;
  }
  stream = await ytdl(queue[0].link, {
    filter: "audioonly",
  });
  dispatcher = await connection.play(stream, {
    type: "opus",
  });
  dispatcher.on("finish", onStreamEnd);
}
// Login to the discord client
client.login(token);
